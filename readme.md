# RB: Active Directory Providers

The Active Directory Providers package allows Umbraco developers to authenticate and authorise Umbraco Users and Members against an Active Directory instance. This package contains instructions to set up all four scenarios and provides two providers to accomplish full Active Directory integration within an Umbraco application.

## Installation

The Active Directory Providers package can be installed via the package's page on [our.umbraco.org](http://our.umbraco.org/projects/developer-tools/active-directory-providers) or via NuGet. If installing via NuGet, use the following package manager command:

    Install-Package RB.ActiveDirectoryProviders

## Configuration

### Member Authentication

To allow Umbraco's front end nodes to authenticate members from Active Directory for protected pages, a new Active Directory Membership provider should be added as follows. A default member type must be created and specified within the membership provider configuration.

    <add name="MyMembersMembershipProvider" 
         type="RB.ActiveDirectoryProviders.ActiveDirectoryUmbracoMembersMembershipProvider, RB.ActiveDirectoryProviders" 
         connectionStringName="MyMembershipConnectionString" 
         connectionUsername="" 
         connectionPassword="" 
         attributeMapUsername="sAMAccountName" 
         defaultMemberType="MemberType" />

This uses a connection string which points to Active Directory.

    <add name="MyMembershipConnectionString" connectionString="LDAP://127.0.0.1/OU=Users,DC=Domain" />

The new membership provider should be selected as the default membership provider within web.config.

    <membership defaultProvider="MyMembersMembershipProvider">

Once setup, Active Directory users will be able to log in as members within Umbraco's front end pages.

### Member Authorisation

To allow the front end to authorise members based on their user groups in Active Directory, a new role provider should be added as follows. A comma separated list of Active Directory groups should be specified.

    <add name="MyMembersRoleProvider" 
         type="RB.ActiveDirectoryProviders.ActiveDirectoryRoleProvider, RB.ActiveDirectoryProviders" 
         connectionStringName="MyMembershipConnectionString" 
         connectionUsername="" 
         connectionPassword="" 
         groupsToUse="Group,Brand" />

The new role provider can use the same connection string as the new membership provider for members.

    <add name="MyMembershipConnectionString" connectionString="LDAP://127.0.0.1/OU=Users,DC=Domain" />

The new role provider should be selected as the default role provider within web.config.

    <roleManager enabled="true" defaultProvider="MyMembersRoleProvider">

Once setup, if an editor were to navigate to the Public Access option for a node, a list of groups from the ‘groupsToUse’ attribute will be displayed. Once selected, a logged in member’s Active Directory groups will be authorised against the node's allowed list to determine if the current member has access or not.

Note: The Active Directory role provider is not tied into Umbraco so can be used for standalone applications.

### User Authentication

To allow Umbraco's back office to authenticate users against Active Directory, a new Active Directory Membership provider should be added as follows. The following membership provider uses .NET's built in Active Directory Membership Provider.

    <add name="MyUsersMembershipProvider" 
         type="System.Web.Security.ActiveDirectoryMembershipProvider, System.Web" 
         connectionStringName="MyMembershipConnectionString" 
         connectionUsername="" 
         connectionPassword="" 
         attributeMapUsername="sAMAccountName" />

The new membership provider can use the same connection string as the new membership provider for members.

    <add name="MyMembershipConnectionString" connectionString="LDAP://127.0.0.1/OU=Users,DC=Domain" />

Once the above has been added, /config/umbracoSettings.config should be amended to use the new provider.

    <providers>
      <users>
        <DefaultBackofficeProvider>MyUsersMembershipProvider</DefaultBackofficeProvider>
      </users>
    </providers>

Once setup, users will be able to log into Umbraco with their Active Directory credentials. A user does not need to enter their domain as part of their username.

### User Authorisation

Umbraco keeps authorisation control so there is no changes needed in order to make Umbraco authorisation work with Active Directory. Upon logging in for the first time with an Active Directory user, the new user is automatically given writer access. To change this, an administrator user must change it in Umbraco or the back office membership provider should be reverted back to Umbraco's in order to login as an administrator and grant the user administrator access.

## Contributing

To raise a new bug, create an [issue](https://bitbucket.org/rbdigital/umbraco-active-directory-providers/issues) on the Bitbucket repository. To fix a bug or add new features or providers, fork the repository and send a [pull request](https://bitbucket.org/rbdigital/umbraco-active-directory-providers/pull-requests) with your changes. Feel free to add ideas to the repository's [issues](https://bitbucket.org/rbdigital/umbraco-active-directory-providers/issues) list if you would to discuss anything related to the package.

## Publishing

Remember to include all necessary files within the package.xml file. Run the following script, entering the new version number when prompted to created a published version of the package:

    Build\Release.bat

The release script will amend all assembly versions for the package, build the solution and create the package file. The script will also commit and tag the repository accordingly to reflect the new version.